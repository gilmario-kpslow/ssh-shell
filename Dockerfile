FROM node:alpine
LABEL manteiner="gilmariosoftware@gmail.com" author="gilmario Batista"
COPY . /usr/src
WORKDIR /usr/src
RUN npm i
EXPOSE 3000
ENTRYPOINT ["node","server.js"]