
let conf;
let socket;
let term;
window.addEventListener('load', function() {
    var terminalContainer = document.getElementById('terminal-container');
    term = new Terminal({ cursorBlink: true });
    term.open(terminalContainer);

    socket = io.connect();
    socket.on('connect', function(e) {
      term.write('\r\n*** Connected to backend***\r\n');

      // Browser -> Backend
      term.onData(function(data) {
        socket.emit('data', data);
      });

      // Backend -> Browser
      socket.on('data', function(data) {
        console.log(data);
        term.writeUtf8(data);
      });

      socket.on('disconnect', function() {
        term.write('\r\n*** Disconnected from backend***\r\n');
      });
    });
  }, false);

document.getElementById('login').addEventListener('click', function() {
  const select = document.getElementById('selectedhost');
  const host = conf.hosts.find(a => a.host === select.value);
  if(host) {
    socket.emit('login', host);
  }
});

document.getElementById('nova').addEventListener('click', function() {
  const a = document.createElement('a');
  a.setAttribute('href', window.location.href);
  a.setAttribute('target', '_blank');
  a.click();

});

document.getElementById('form').addEventListener('submit', function(event) {
  event.preventDefault();
  event.stopPropagation();
  const host = document.getElementById('host');
  const username = document.getElementById('username');
  const password = document.getElementById('password');
  const port = document.getElementById('port');
  const nome = document.getElementById('nome');
  conf.hosts.push({host: host.value, username: username.value, password: password.value, port: port.value, nome: nome.value});
  host.value = '';
  username.value = '';
  password.value = '';
  port.value = 22;
  term.write("Servidor registrado.");
  saveConf();
  getHosts();
});


function getHosts() {
  const select = document.getElementById('selectedhost');
  while(select.firstChild) {
    select.removeChild(select.firstChild);
  }
  conf.hosts.forEach(e => {
    const option = document.createElement('option');
    console.log(e);
    option.text = e.nome;
    option.value = e.host;
    select.appendChild(option);
  });
}

function loadConf() {
  confString = localStorage.getItem('configuracoes');
  if (confString) {
    conf = JSON.parse(confString);
  } else {
    conf = {};
    conf.hosts = [];
    saveConf();
  }
}

function saveConf(){
  localStorage.setItem('configuracoes', JSON.stringify(conf));
}

loadConf();
getHosts();