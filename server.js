const fs = require('fs');
const path = require('path');
const server = require('http').createServer(onRequest);

const io = require('socket.io')(server);
const SSHClient = require('ssh2').Client;
const staticFiles = {};
const basePath = path.join(require.resolve('xterm'), '../..');
[ 
  'css/xterm.css',
  'lib/xterm.js'
].forEach(function(f) {
  staticFiles['/' + f] = fs.readFileSync(path.join(basePath, f));
});
staticFiles['/'] = fs.readFileSync('index.html');
staticFiles['/index.js'] = fs.readFileSync('resources/index.js');
// staticFiles['/hosts'] = fs.readFileSync('hostslist.json');
staticFiles['/style.css'] = fs.readFileSync('resources/style.css');
var fitPath = path.join(require.resolve('xterm-addon-fit'));
staticFiles['/lib/xterm-addon-fit.js'] = fs.readFileSync(fitPath);
// staticFiles['/login/index.js'] = fs.readFileSync('login/index.js');
// staticFiles['/login/style.css'] = fs.readFileSync('login/style.css');
// staticFiles['/login'] = fs.readFileSync('login/index.html');

// function updateHostname() {
//   const hostsData = fs.readFileSync('hosts.json');
//   const hosts = JSON.parse(hostsData);
//   const hostlist = hosts.map(function (h) { return h.host; });
//   fs.writeFileSync('hostslist.json', JSON.stringify(hostlist));
//   staticFiles['/hosts'] = fs.readFileSync('hostslist.json');
// }

function onRequest(req, res) {
  var file;
  // Verificar se o cara ta logado...
  if (req.method === 'GET' && (file = staticFiles[req.url])) {
    res.writeHead(200, {
      'Content-Type': 'text/' + (/css$/.test(req.url) ? 'css': (/js$/.test(req.url) ? 'javascript' : 'html'))
    });
    return res.end(file);
  }
  res.writeHead(404);
  res.end();
}

io.on('connect', function(socket) {
  socket.on('login', function(data) {
    if(data) {
      sshConnect(data, socket);
    }
  });
});

function sshConnect(host, socket) {
  var conn = new SSHClient();
  conn.on('keyboard-interactive', (name, instructions, instructionsLang, prompts, finish) => { 
      finish([host.password]); 
  }).on('ready', function() {
    socket.emit('data', '\r\n*** SSH CONNECTION ESTABLISHED ***\r\n');
    conn.shell(function(err, stream) {
      if (err)
        return socket.emit('data', '\r\n*** SSH SHELL ERROR: ' + err.message + ' ***\r\n');
      socket.on('data', function(data) {
        if(conn) {
          stream.write(data, 'UTF-8');
        }
      });
      stream.on('data', function(d) {
        socket.emit('data', d.toString('UTF-8'));
  }).on('close', function() {
        conn.end();
        conn = undefined;
        //conn.finish();
      });
    });
  }).on('close', function() {
    socket.emit('data', '\r\n*** SSH CONNECTION CLOSED ***\r\n');
  }).on('error', function(err) {
    socket.emit('data', '\r\n*** SSH CONNECTION ERROR: ' + err.message + ' ***\r\n');
  }).connect({
    host: host.host,
    port: host.port,
    username: host.username,
    password: host.password,
    tryKeyboard : true
  });
}

server.listen(3000);
